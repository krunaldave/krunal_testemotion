package EmotionRecord;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.pmw.tinylog.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;

public class TestCase1
{

	public static AndroidDriver<AndroidElement> andDriver;
	public static PageObjects pom;

	@BeforeClass public void setup() throws MalformedURLException {
		andDriver = BaseCapabilities.getAndroidSetup();
		pom = new PageObjects(andDriver);
		Logger.info("Starting app");
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 0) public void startApp() {
		Assert.assertTrue(pom.loginButtons.size() > 0);
		for (int i = 0; i < pom.loginButtons.size(); i++) {
			if(pom.loginButtons.get(i).getText().equalsIgnoreCase("Log in")) {
				pom.loginButtons.get(i).click();
				break;
			}
		}
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(dependsOnMethods = { "startApp" }) public void addCredentials() {
		String userName = "estest@emotion.com";
		String password = "abcd1234";
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertTrue(pom.credentialsEditTexts.size() > 0);
		for (int i = 0; i < pom.credentialsEditTexts.size(); i++) {
			if(i == 0) {
				pom.credentialsEditTexts.get(i).clear();
				pom.credentialsEditTexts.get(i).sendKeys(userName);
			} else if(i == 1) {
				pom.credentialsEditTexts.get(i).clear();
				pom.credentialsEditTexts.get(i).sendKeys(password);
			}
		}
		Assert.assertNotNull(pom.loginButton);
		pom.loginButton.click();
	}

	@Test(dependsOnMethods = { "addCredentials" }) public void selectEmotion() {
		String emotion = "Happy";
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertTrue(pom.emotions.size() > 0);
		tapOnItemFromList(pom.emotions, emotion);
	}

	@Test(dependsOnMethods = { "selectEmotion" }) public void pressNext() {
		Assert.assertNotNull(pom.seekBar);
		new TouchAction(andDriver)
				.tap(point(pom.seekBar.getLocation().getX(), (int) (pom.seekBar.getSize().getHeight() * 0.4)))
				.perform();
		Assert.assertNotNull(pom.selectIntensity);
		pom.selectIntensity.click();
	}

	@Test(dependsOnMethods = { "pressNext" }) public void addFeel() {
		Assert.assertNotNull(pom.causeCheckMark);
		pom.causeCheckMark.click();
	}

	@Test(dependsOnMethods = { "addFeel" }) public void selectFeeling() {
		String feeling = "It's my treatment";
		Assert.assertTrue(pom.feelings.size() > 0);
		for (int i = 0; i < pom.feelings.size(); i++) {
			if(pom.feelings.get(i).getText().equalsIgnoreCase(feeling)) {
				new TouchAction(andDriver).tap(tapOptions().withElement(element(pom.feelings.get(i)))).perform();
				break;
			}
		}
		tapOnItemFromList(pom.feelings, feeling);
		Assert.assertNotNull(pom.moreOptions);
		pom.moreOptions.click();
	}

	public void tapOnItemFromList(List<AndroidElement> elementList, String choice) {
		for (int i = 0; i < elementList.size(); i++) {
			if(elementList.get(i).getText().equalsIgnoreCase(choice)) {
				new TouchAction(andDriver).tap(tapOptions().withElement(element(elementList.get(i)))).perform();
				break;
			}
		}
	}

	@Test(dependsOnMethods = { "selectFeeling" }) public void selectTreatment() {
		String reason = "I'm starting a new treatment cycle.";
		Assert.assertTrue(pom.reasons.size() > 0);
		tapOnItemFromList(pom.reasons, reason);
		Assert.assertNotNull(pom.doneButton);
		pom.doneButton.click();
	}

	@Test(dependsOnMethods = { "selectTreatment" }) public void finishUp() {
		Assert.assertNotNull(pom.finishUp);
		pom.finishUp.click();
	}

	/*
		 Hamburg menu was unidentified with all locators. so making it work using touch actions and coordinates.
	 */

	@Test(dependsOnMethods = { "finishUp" }) public void openMenu() {

		String profile = "My profile";
		Dimension dim = andDriver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int startX = (int) (width * 0.001);
		int top_y = (int) (height * 0.05);

		new TouchAction(andDriver).tap(point(startX, top_y)).perform();
		Assert.assertTrue(pom.menuItems.size() > 0);
		tapOnItemFromList(pom.menuItems, profile);
	}

	/*
		 ALL LOCATOR STRATEGIES FAILED TO IDENTIFY OBJECTS,
		 SO USING TOUCH ACTIONS DUE TO MAY BE A FRAGMENT IMPLEMENTATION FOR THE SCREENS WHICH NOT STABLE BY APPIUM.
	 */

	@Test(dependsOnMethods = { "openMenu" }) public void accountSignOut() {

		WebDriverWait wait = new WebDriverWait(andDriver, 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ProgressBar")));

		Dimension dim = andDriver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int startX = (int) (width * 0.25);
		int bottom_y = (int) (height * 0.45);
		new TouchAction(andDriver).tap(point(startX, bottom_y)).perform();

		Assert.assertNotNull(pom.signOut);
		pom.signOut.click();

		Assert.assertNotNull(pom.popUpLogOut);
		pom.popUpLogOut.click();

	}

	@AfterClass public void tearDown() {
		Logger.info("Closing app");
		andDriver.pressKey(new KeyEvent(AndroidKey.BACK));
	}
}

