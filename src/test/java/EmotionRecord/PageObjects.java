package EmotionRecord;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.pmw.tinylog.Logger;

import java.util.List;

public class PageObjects
{
	public PageObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		Logger.info("I am in PageObject");
	}

	@CacheLookup @AndroidFindBy(className = "android.widget.Button") public List<AndroidElement> loginButtons;

	@CacheLookup @AndroidFindBy(className = "android.widget.EditText") public List<AndroidElement> credentialsEditTexts;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_login_next")
	public AndroidElement loginButton;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/text_view_emotion_selection_item_name")
	public List<AndroidElement> emotions;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/seekBar_intensity") public AndroidElement seekBar;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_select_intensity")
	public AndroidElement selectIntensity;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/image_cause_check_mark")
	public AndroidElement causeCheckMark;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/text_view_reason_parent_item_name")
	public List<AndroidElement> feelings;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_see_more_option")
	public AndroidElement moreOptions;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/checkbox_reason_child_item")
	public List<AndroidElement> reasons;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_done_widget") public AndroidElement doneButton;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_done_widget") public AndroidElement finishUp;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/text_view_item_name")
	public List<AndroidElement> menuItems;

	@CacheLookup @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_view_sign_out") public AndroidElement signOut;

	@CacheLookup @AndroidFindBy(id = "android:id/button1") public AndroidElement popUpLogOut;

}
